## Difficulté rencontré

# Partie 1 [jsfidlle]([lien](http://jsfiddle.net/Yaya1543/hp0ta29q/)

Utilisation de l'api wolfram|alpha difficulté à trouver comment s'en servir pour
ce qui à été demandé. Car l'api est très riche et réalise de nombreuses choses.

Les requêtes à une api en http sont bloqué par le navigateur si jsfiddle est utilisé
en version https. Solution utiliser la version http du site.

![WolframApiPosibility](screenshot/wolframApi.PNG)

Essai de l'ajout du marqueur sans succées. Ce qui à était compris il faut ajouter
le point sur une couche de la map (layers) pour cela ont utilise un marqueur (marker).

Imposibilité d'utiliser l'api sans clée api. Nous avons donc créer un compte factice
pour générer une clée api. Mais sans succés nous pouvons obtenir les informations directement
via le navigateur pas via l'exercice sur jsfiddle. (erreur cross origin) le serveur de l'api
nous l'interdit.

# Partie 2

Nous avons eu un problème de version java nous le projet n'est pas compatible
avec java 11 il est compatible avec java 8 car le package javax n'est plus
disponible dans la version 11. Solution passer à la version 8. Recommandation
de la part de nos enseignant. Ajout de message pour voir que le programme
à bien démarré.

# Partie 3

Problème de version java. Problème choix retour controlleur.