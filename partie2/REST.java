import java.io.StringReader;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.http.HTTPBinding;

@WebServiceProvider
@ServiceMode(value = Service.Mode.PAYLOAD)
public class REST implements Provider<Source> {

  private String msg;

  REST(String msg) {
    this.msg = msg;
  }

  public Source invoke(Source source) {
    StreamSource reply = new StreamSource(
      new StringReader("<p>" + msg + "</p>")
    );
    return reply;
  }
  
  public static void main(String args[]) {
    System.out.println("Starting...");

    Endpoint e1 = Endpoint.create(HTTPBinding.HTTP_BINDING, new REST("Réponse du service Rest"));
    e1.publish("http://127.0.0.1:8084/hello/world");

    Endpoint e2 = Endpoint.create(HTTPBinding.HTTP_BINDING, new REST("Université de Rouen"));
    e2.publish("http://127.0.0.1:8090/test");

    System.out.println("Loaded...");
  }
}
