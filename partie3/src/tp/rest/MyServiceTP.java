package tp.rest;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Source;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP implements Provider<Source> {

    public final static String url = "http://127.0.0.1:8084/";

    public static void main(String args[]) {
        Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP());

        e.publish(url);
        System.out.println("Service started, listening on " + url);
        // pour arrêter : e.stop();
    }

    private JAXBContext jc;

    @javax.annotation.Resource(type = Object.class)
    protected WebServiceContext wsContext;

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

    public MyServiceTP() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Exception " + je);
            throw new WebServiceException("Cannot create JAXBContext", je);
        }

        // Fill our center with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));
    }

    public Source invoke(Source source) {
        MessageContext mc = wsContext.getMessageContext();
        String path = (String) mc.get(MessageContext.PATH_INFO);
        String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);

        // determine the targeted ressource of the call
        try {
            // no target, throw a 404 exception.
            if (path == null) {
                throw new HTTPException(404);
            }
            // "/animals" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("animals")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 :
                        return this.animalsCrud(method, source);
                    case 2 :
                        return this.animalCrud(method, source, path_parts[1]);
                    case 3 :
                        // return this.animalInfoWolf(method, path_parts[1]);
                    default:
                        throw new HTTPException(404);
                }
            }
            else if (path.startsWith("find/")) {
            	String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 3:
                    	switch (path_parts[1]){
                    		case "byName" :
                                return this.animalFindByName(method, path_parts[2]);
                    		case "at" :
                    			return this.animalAtPosition(method, path_parts[2]);
                    		case "near" :
                    			return this.animalNearPosition(method,path_parts[2]);
                    		default :
                    			throw new HTTPException(404);
                    	}
                    default:
                        throw new HTTPException(404);
                }
            }
            else if ("coffee".equals(path)) {
                throw new HTTPException(418);
            }
            else {
                throw new HTTPException(404);
            }
        } catch (JAXBException e) {
            throw new HTTPException(500);
        }
    }

    /**
     * Method bound to calls on /animals/{something}
     */
    private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
        //GET /animals/{animal_id}   (retourne l'animal l'identifiant {animal_id} passer dans l'url)
    	if("GET".equals(method)){
            try {
                return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        }
    	// POST /animals/{animal_id}  (ajoute l'animal d'identifant {animal_id})
        else if("POST".equals(method)){
        	Animal animal = null;
        	
        	// vérifier qu'aucun animal du center n'a ce identifiant
        	try {
                animal = center.findAnimalById(UUID.fromString(animal_id));
                if(animal != null)
                {
                	// conflit un animal existe avec le même identifiant
                	throw new HTTPException(409);
                }
            } catch (AnimalNotFoundException e) {
               // récupérer les information de l'animal à partir de la source
            	animal = this.unmarshalAnimal(source);
            }
        	
        	animal.setId(UUID.fromString(animal_id));
        	final String cageName = animal.getCage();
        	//on ajoute l'animal dans sa sage correspondante
        	this.center.getCages()
	            .stream()
	            .filter(cage -> cage.getName().equals(cageName))
	            .findFirst()
	            .orElseThrow(() -> new HTTPException(404))
	            .getResidents()
	            .add(animal);
        	//retourner l'ensemble des animaux après ajout du nouvel animal
        	return new JAXBSource(this.jc, this.center);
        }
    	//PUT /animals/{animal_id} (modifie les informations de l'animal avec l'identifiant {animal_id})
        else if("PUT".equals(method)){
        	Animal animal = null;
        	
        	//vérifier que l'animal existe dans le center
        	try {
                	animal = center.findAnimalById(UUID.fromString(animal_id));
                
            } catch (AnimalNotFoundException e) {
               //animal non  identifié dans le center
            	throw new HTTPException(404);
            }
        	// récupérer les information de l'animal à partir de la source
        	animal = this.unmarshalAnimal(source);
        	
        	//vérifier que les identifiants sont uniques
        	if(!animal.getId().equals(UUID.fromString(animal_id)))
        	{
        		throw new HTTPException(400);
        	}
        	
        	final String cageName = animal.getCage();
        	//supprimer l'animal afin de le remplacer par les nouvelles modifications
			this.center.getCages()
                .stream()
                .filter(c -> c.getName().equals(cageName))
                .findFirst()
                .orElseThrow(() -> new HTTPException(404))
                .getResidents()
                .removeIf(a -> a.getId().equals(UUID.fromString(animal_id)));
			// ajouter l'animal avec les modifications
			this.center.getCages()
	            .stream()
	            .filter(c -> c.getName().equals(cageName))
	            .findFirst()
	            .orElseThrow(() -> new HTTPException(404))
	            .getResidents()
	            .add(animal);
			
        	//retourner l'ensemble de animaux
            return new JAXBSource(this.jc, this.center);
        }
    	//DELETE /animals/{animal_id} (supprime  l'animal avec l'identifiant {animal_id})
        else if("DELETE".equals(method)){
        	Animal animal = null;
        	
        	//vérifier que l'animal existe dans le center
        	try {
                	animal = center.findAnimalById(UUID.fromString(animal_id));
                
            } catch (AnimalNotFoundException e) {
               //animal non  identifie dans le center
            	throw new HTTPException(404);
            }
        	
        	final String cageName = animal.getCage();
        	//supprimer l'animal afin de le remplacer par les nouvelles modifications
			this.center.getCages()
                .stream()
                .filter(c -> c.getName().equals(cageName))
                .findFirst()
                .orElseThrow(() -> new HTTPException(404))
                .getResidents()
                .removeIf(a -> a.getId().equals(UUID.fromString(animal_id)));
			
        	//retourner l'ensemble de animaux
            return new JAXBSource(this.jc, this.center);
        }
        else{
            throw new HTTPException(405);
        }
    }

    /**
     * Method bound to calls on /animals
     */
    private Source animalsCrud(String method, Source source) throws JAXBException {
    	
    	//	GET /animals   (retourne l'ensemble des animaux du zoo)
        if("GET".equals(method)){
            return new JAXBSource(this.jc, this.center);
        }
        // POST /animals   (ajoute un nouvel animal dans le center)
        else if("POST".equals(method)){
            Animal animal = unmarshalAnimal(source);
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new JAXBSource(this.jc, this.center);
        }
        // PUT /animals (modifie l'ensemble des animaux du center)
        else if("PUT".equals(method)){
        	// récupérer les cages contenant les animaux à modifié
        	Collection<Cage> listModifCages = this.unmarshalCenter(source).getCages();
        	  	
        	//effectuer les modification nécessaires
	    	for(Cage cage: listModifCages)
	    	{
	    		for(Animal animal: cage.getResidents()) 
	    		{
	    			//supprimer l'animal afin de le remplacer par les nouvelles modifications
	    			this.center.getCages()
	                    .stream()
	                    .filter(c -> c.getName().equals(animal.getCage()))
	                    .findFirst()
	                    .orElseThrow(() -> new HTTPException(404))
	                    .getResidents()
	                    .removeIf(a -> a.getId().equals(animal.getId()));
	    			
	    			// ajouter l'animal avec les modifications
	    			this.center.getCages()
                    .stream()
                    .filter(c -> c.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
	                   	
	    		}
	    	}
	    	//retourner l'ensemble des animaux après modifications
	    	return new JAXBSource(this.jc, this.center);
        }
        // DELETE /animals (supprimer l'ensemble des animaux du center)
        else if("DELETE".equals(method)){
        	
        	Collection<Cage> listCages = this.center.getCages();
            Cage cage;
            Collection<Animal> listAnimals;
            Iterator<Cage> iterator = listCages.iterator();
            
            
            while(iterator.hasNext()){
            	cage = iterator.next();
            	listAnimals = cage.getResidents();
            	listAnimals.clear();
            }
            
            // retourner les cages vides sans animaux
            return new JAXBSource(this.jc, this.center);
        }
        else{
            throw new HTTPException(405);
        }
    }
    /**
     * Method bound to calls on /find/byName/{name}
     */
    private Source animalFindByName(String method, String animal_name) throws JAXBException {
    	
    	// GET /find/byName/{name} (Recherche d'un animal par son nom)
        if("GET".equals(method)){
        	//vérifier que le nom de l'animal n'est pas vide
        	if(animal_name.isEmpty())
        	{
        		//mauvaise requête
        		throw new HTTPException(404); 
        	}
        	//l'animal à retourner
        	Animal animal = null;
        	
        	//récupérer l'ensemble des cages
        	Collection<Cage> listCage = this.center.getCages();
        	for(Cage cage: listCage)
        	{
        		//récupérer l'ensemble des animaux de la cage courante
        		Collection<Animal> listAnimal = cage.getResidents();
        		//parcourir la liste pour chercher l'animal
        		for(Animal a: listAnimal)
        		{
        			if(animal_name.toLowerCase().equals(a.getName().toLowerCase()))
        			{
        				animal = a;
        				break;
        			}
        		}
        	}
        	//aucun animal trouvé
        	if(animal == null)
        	{
        		throw new HTTPException(404);
        	}
        	
        	return new JAXBSource(this.jc,animal );
        }       
        else{
            throw new HTTPException(405);
        }
    }

    /**
     * Method bound to calls on /find/at/{position}
     */
    private Source animalAtPosition(String method, String position) throws JAXBException {
    	// GET /find/at/{position} (return le premier animal se trouvant à cette position
        if("GET".equals(method)){
        	//position est une String constituée par la latitude et la longitude séparée par une virgule
        	// expemle /find/at/49.305,1.2157357
        	
            String[] strPosition = position.split(",");
            if(strPosition.length != 2)
            {
            	//mauvaise rquête
            	throw  new HTTPException(400);
            }
           // créer la position correspondante
            Position pos = new Position(Double.parseDouble(strPosition[0]), Double.parseDouble(strPosition[1]));
            
            // l'animal à retourner
            Optional<Animal> animal;
            animal = this.center.getCages()
			            .stream()
			            .filter(cage -> cage.getPosition().equals((pos)))
			            .findFirst()
			            .orElseThrow(() -> new HTTPException(404))
			            .getResidents()
			            .stream()
			            .findFirst();
           if(!animal.isPresent())
           {
        	   //aucun animal trouvé
        	   throw  new HTTPException(404);
           }
           // retourner l'animal trouvé
           return new JAXBSource(this.jc,animal.get());
        }      
        else{
            throw new HTTPException(405);
        }
    }
    
    /**
     * Method bound to calls on /find/near/{position}
     */
    private Source animalNearPosition(String method, String position) throws JAXBException {
    	// GET /near/at/{position} (return les animaux se trouvant à cette position
        if("GET".equals(method)){
        	//position est une String constituée par la latitude et la longitude séparée par une virgule
        	// expemle /find/near/49.305,1.2157357
        	
            String[] strPosition = position.split(",");
            if(strPosition.length != 2)
            {
            	//mauvaise rquête
            	throw  new HTTPException(400);
            }
           // créer la position correspondante
            Position pos = new Position(Double.parseDouble(strPosition[0]), Double.parseDouble(strPosition[1]));
            
            // récupérer la cage contenant l'ensemble des animaux à cette position
            Cage cage = null;
            cage = this.center.getCages()
			            .stream()
			            .filter(c -> c.getPosition().equals((pos)))
			            .findFirst()
			            .orElseThrow(() -> new HTTPException(404));
			            
			            
           if((cage == null) || (cage.getResidents().isEmpty()))
           {
        	   //aucun animal trouvé à cette position
        	   throw  new HTTPException(404);
           }
           // retourner les animaux  trouvés
           return new JAXBSource(this.jc, cage);
        }      
        else{
            throw new HTTPException(405);
        }
    }


    // private Animal animalInfoWolf(String method, String position) {

    // }
    
    private Animal unmarshalAnimal(Source source) throws JAXBException {
        return (Animal) this.jc.createUnmarshaller().unmarshal(source);
    }
    private Center unmarshalCenter(Source source) throws JAXBException {
        return (Center) this.jc.createUnmarshaller().unmarshal(source);
    }
    
}
